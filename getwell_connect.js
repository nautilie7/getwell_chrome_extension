$(document).ready(function(){
  //***INITIAL SETUP STEP 1**//

  /*PROD*/
  //var api_key = '1eeadb6c0919df1121cdc4e39ef5de2a';
  var gws_version = "0.1"
  //var getwell_url = 'https://getwell.herokuapp.com';
  //DEV*/
  var api_key = '48c6f044b471a26c5d72cb71074862fe'
  var getwell_url = 'http://localhost:3000'

  //STEP 2
  //Post to Getwell
   $(document).on('click', "#post_getwell", function(){
    //**POSTING SETUP**//
    //** REPLACE '#d1-d8' '#t1-t8' '#rx1-rx8' WITH ELEMENTS FOR **
    //** DIAGNOSIS COES 1-8, TREATMENT PLANS 1-8, AND DRUGS     **
    //** ASSOCIATED WITH TREATMENT PLAN 1-8. RESPECTIVELY.      **
    d1_val = $("#d1").val();
    d2_val = $("#d2").val();
    d3_val = $("#d3").val();
    d4_val = $("#d4").val();
    d5_val = $("#d5").val();
    d6_val = $("#d6").val();
    d7_val = $("#d7").val();
    d8_val = $("#d8").val();
    t1_val = $("#t1").val();
    t2_val = $("#t2").val();
    t3_val = $("#t3").val();
    t4_val = $("#t4").val();
    t5_val = $("#t5").val();
    t6_val = $("#t6").val();
    t7_val = $("#t7").val();
    t8_val = $("#t8").val();
    rx1_val = $("#rx1").val();
    rx2_val = $("#rx2").val();
    rx3_val = $("#rx3").val();
    rx4_val = $("#rx4").val();
    rx5_val = $("#rx5").val();
    rx6_val = $("#rx6").val();
    rx7_val = $("#rx7").val();
    rx8_val = $("#rx8").val();
    
    //post
     $.ajax({
      beforeSend: function (xhr) {
      xhr.setRequestHeader ('Authorization', api_key);
      },
      headers: { 
        Accept : "application/json",
        "Content-Type": "application/json"
    },
    url: getwell_url+'/emr/submit',
    data : JSON.stringify({"d1": d1_val, "d2": d2_val, "d3": d3_val, "d4": d4_val, "d5": d5_val, "d6": d6_val, "d7": d7_val, "d8": d8_val, "tplan1": t1_val, "tplan2": t2_val, "tplan3": t3_val, "tplan4": t4_val, "tplan5": t5_val, "tplan6": t6_val, "tplan7": t7_val, "tplan8": t8_val, "rx1": rx1_val, "rx2": rx2_val, "rx3": rx3_val, "rx4": rx4_val, "rx5": rx5_val, "rx6": rx6_val, "rx7": rx7_val, "rx8": rx8_val }),
    contentType : 'application/json',
    type : 'POST',
    accept : 'application/json',
    crossDomain: true,
    success: function(){

      //uncomment this to check if getwell is successfully posting
      //console.log('Successfully posted to GetWell :)');
    },
    error: function(xhr){
      if (xhr.responseText != ""){
      console.error("GetWell responded with: " + xhr.responseText);
    }
    else
    {
      console.log("Unauthorized client. Unable to post. Please contact GetWell")
    }
    }
    });

    return false;

   });



  //Query Getwell
  $(document).on('click', "#query_getwell", function(){

    //**QUERY SETUP**//
    //** REPLACE '#d1-d8' WITH ELEMENTS FOR **
    //** DIAGNOSIS COES 1-8, RESPECTIVELY   **

  	d1_val = $("#d1").val();
  	d2_val = $("#d2").val();
  	d3_val = $("#d3").val();
  	d4_val = $("#d4").val();
  	d5_val = $("#d5").val();
  	d6_val = $("#d6").val();
  	d7_val = $("#d7").val();
  	d8_val = $("#d8").val();
  	$('#display_getwell').html('');
    $.ajaxPrefilter('script', function(options) {
      options.cache = true;
      });
    $.ajax({
          beforeSend: function (xhr) {
              xhr.setRequestHeader ('Authorization', api_key);
          },
          dataType: 'html',
          type: 'GET',
          url: getwell_url+'/gwsearch/ajax_search?d1='+d1_val+'&d2='+d2_val+'&d3='+d3_val+'&d4='+d4_val+'&d5='+d5_val+'&d6='+d6_val+'&d7='+d7_val+'&d8='+d8_val+'&gws_version='+gws_version,
          crossDomain: true,
          success:function(result){
        
            console.log('GetWell initialized...')
            //$("#display_getwell").html(result);
            $("#display_getwell").html(result)

          },
          error: function(result) {
            $('#display_getwell').html('Unauthorized client. Please contact GetWell');
          }
  
        });
    });
  
  //fsfsadfsdfsadf
  //Query Getwell for admin page show
$(document).on('click', ".query-getwell-admin", function(){

  var diagcodeRow = $(this).closest('tr.diagcode-row');

  var diagcodeCell = diagcodeRow.children('.diagcode-cell');

  var diagcodeLabels = diagcodeRow.children('span.label');

  var dxarray = [];

  diagcodeCell.children('span').each(function(){

    dxarray.push($(this).text());    

  });

    //**QUERY SETUP**//
    //** REPLACE '#d1-d8' WITH ELEMENTS FOR **
    //** DIAGNOSIS COES 1-8, RESPECTIVELY   **

    var dxArray = [];
    var dxCode1 = $('input[id*="txtDiagnoseCode1"]');
    var dxCode2 = $('input[id*="txtDiagnoseCode2"]');
    var dxCode3 = $('input[id*="txtDiagnoseCode3"]');
    var dxCode4 = $('input[id*="txtDiagnoseCode4"]');
    var dxCode5 = $('input[id*="txtDiagnoseCode5"]');
    var dxCode6 = $('input[id*="txtDiagnoseCode6"]');
    var dxCode7 = $('input[id*="txtDiagnoseCode7"]');
    var dxCode8 = $('input[id*="txtDiagnoseCode8"]');

    if(dxCode1.val() != 0 && dxCode1.val() != ""){
      dxArray.push(dxCode1.val());
    }
    if(dxCode2.val() != 0 && dxCode2.val() != ""){
      dxArray.push(dxCode2.val());
    }
    if(dxCode3.val() != 0 && dxCode3.val() != ""){
      dxArray.push(dxCode3.val());
    }
    if(dxCode4.val() != 0 && dxCode4.val() != ""){
      dxArray.push(dxCode4.val());
    }
    if(dxCode5.val() != 0 && dxCode5.val() != ""){
      dxArray.push(dxCode5.val());
    }
    if(dxCode6.val() != 0 && dxCode6.val() != ""){
      dxArray.push(dxCode6.val());
    }
    if(dxCode7.val() != 0 && dxCode7.val() != ""){
      dxArray.push(dxCode7.val());
    }
    if(dxCode8.val() != 0 && dxCode8.val() != ""){
      dxArray.push(dxCode8.val());
    }

    d1_val = dxArray[0] || '';
    d2_val = dxArray[1] || '';
    d3_val = dxArray[2] || '';
    d4_val = dxArray[3] || '';
    d5_val = dxArray[4] || '';
    d6_val = dxArray[5] || '';
    d7_val = dxArray[6] || '';
    d8_val = dxArray[7] || '';

    console.log('d1='+d1_val+'&d2='+d2_val+'&d3='+d3_val+'&d4='+d4_val+'&d5='+d5_val+'&d6='+d6_val+'&d7='+d7_val+'&d8='+d8_val);

    $('#display_getwell').html('');

    $.ajaxPrefilter('script', function(options) {
      options.cache = true;
    });

    $.ajax({
        beforeSend: function (xhr) {
            xhr.setRequestHeader ('Authorization', api_key);
        },
        dataType: 'html',
        type: 'GET',
        url: getwell_url+'/gwsearch/ajax_search?d1='+d1_val+'&d2='+d2_val+'&d3='+d3_val+'&d4='+d4_val+'&d5='+d5_val+'&d6='+d6_val+'&d7='+d7_val+'&d8='+d8_val+'&gws_version='+gws_version,
        crossDomain: true,
        success:function(result){      
          console.log('GetWell initialized...')
          //$("#display_getwell").html(result);
          $("#display_getwell").html(result)
        },
        error: function(result) {
          $('#display_getwell').html('Unauthorized client. Please contact GetWell');
        }  
      });
    });
});


