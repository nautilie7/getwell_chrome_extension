$(function(){

	$(".fancybox").fancybox({
        showNavArrows : false,
        autoSize : false,
        autoHeight : true,
        beforeLoad : function() {    
            this.width  = parseInt(this.element.data('fancybox-width')); 
        }
    });

    //check if diagnosis are available
    var dxArray = [];
    var dxCode1 = $('input[id*="txtDiagnoseCode1"]');
    var dxCode2 = $('input[id*="txtDiagnoseCode2"]');
    var dxCode3 = $('input[id*="txtDiagnoseCode3"]');
    var dxCode4 = $('input[id*="txtDiagnoseCode4"]');
    var dxCode5 = $('input[id*="txtDiagnoseCode5"]');
    var dxCode6 = $('input[id*="txtDiagnoseCode6"]');
    var dxCode7 = $('input[id*="txtDiagnoseCode7"]');
    var dxCode8 = $('input[id*="txtDiagnoseCode8"]');

    if(dxCode1.val() != 0 && dxCode1.val() != ""){
    	dxArray.push(dxCode1.val());
    }
    if(dxCode2.val() != 0 && dxCode2.val() != ""){
    	dxArray.push(dxCode2.val());
    }
    if(dxCode3.val() != 0 && dxCode3.val() != ""){
    	dxArray.push(dxCode3.val());
    }
    if(dxCode4.val() != 0 && dxCode4.val() != ""){
    	dxArray.push(dxCode4.val());
    }
    if(dxCode5.val() != 0 && dxCode5.val() != ""){
    	dxArray.push(dxCode5.val());
    }
    if(dxCode6.val() != 0 && dxCode6.val() != ""){
    	dxArray.push(dxCode6.val());
    }
    if(dxCode7.val() != 0 && dxCode7.val() != ""){
    	dxArray.push(dxCode7.val());
    }
    if(dxCode8.val() != 0 && dxCode8.val() != ""){
    	dxArray.push(dxCode8.val());
    }


    //check if diagnoses are present
    if (dxArray.length > 0){
    	
    }

    $('head').append('<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">');
    $('body').append("<a href='#display_getwell' id='getwell_button' class='fancybox query-getwell-admin' data-fancybox-width='950' >Getwell</a>");
	$('body').append("<div id='display_getwell'></div>");

    $('#getwell_button').click(function(){
    	console.log("dx1 = " + $('input[id*="txtDiagnoseCode1"]').val());
    	console.log("dx2 = " + $('input[id*="txtDiagnoseCode2"]').val());
    	console.log("dx3 = " + $('input[id*="txtDiagnoseCode3"]').val());
    	console.log("dx4 = " + $('input[id*="txtDiagnoseCode4"]').val());
    	console.log("dx5 = " + $('input[id*="txtDiagnoseCode5"]').val());
    	console.log("dx6 = " + $('input[id*="txtDiagnoseCode6"]').val());
    	console.log("dx7 = " + $('input[id*="txtDiagnoseCode7"]').val());
    	console.log("dx8 = " + $('input[id*="txtDiagnoseCode8"]').val());
    });

    console.log(dxArray.length)	

});



chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
	console.log(sender.tab ?
	    "from a content script:" + sender.tab.url :
	    "from the extension:" + request.greeting);
	if (request.greeting == "hello")
	  sendResponse({farewell: "goodbye"});
});